﻿using UnityEngine;
using System.Collections;

public class TetrisCivilian : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		RaycastHit2D[] neighbors = Physics2D.CircleCastAll(transform.position, 0.2f,new Vector2(0,0),0.5f);
		
		int numNeighbors = 0;
		
		
		foreach (RaycastHit2D enemy in neighbors){
			if(enemy.collider.tag=="Civilian"){
				numNeighbors++;
				
				Debug.Log (numNeighbors);
			}
			
		}
		if (numNeighbors==3){
			foreach (RaycastHit2D enemy in neighbors){
				if(enemy.collider.tag=="Civilian"){
					Sound.playSound=true; 
					Destroy (enemy.collider.gameObject);
					EnemySpawner.score ++; 
				}
				Destroy (gameObject);}
		}
	}
}
