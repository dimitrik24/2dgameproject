﻿using UnityEngine;
using System.Collections;

public class TetrisPolice : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		RaycastHit2D[] neighbors = Physics2D.CircleCastAll(transform.position, 0.2f,new Vector2(0,0),0.5f);
		
		int numNeighbors = 0;
		
		
		foreach (RaycastHit2D enemy in neighbors){
			if(enemy.collider.tag=="Police"){
				numNeighbors++;
				
				Debug.Log (numNeighbors);
			}
			
		}
		if (numNeighbors==3){
			foreach (RaycastHit2D enemy in neighbors){
				if(enemy.collider.tag=="Police"){
					Sound.playSound=true; 
					Destroy (enemy.collider.gameObject);
					EnemySpawner.score ++; 
				}
				Destroy (gameObject);}
		}
	}
}
