﻿using UnityEngine;
using System.Collections;

public class Particles : MonoBehaviour {

	public ParticleSystem particles; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	

	}


	public void particlePlay(GameObject posXY){
		Instantiate (particles, posXY.transform.position, Quaternion.identity); 
		particles.Play(); 
		//particles.transform.position = posXY.position; 
		StartCoroutine("particlesStop");
	}

	 IEnumerator particlesStop(){
		yield return new WaitForSeconds(1f);
		particles.Stop(); 
		Destroy (particles); 
	}
}
