﻿using UnityEngine;
using System.Collections;

public class carController : MonoBehaviour {

    public float speed = 1.5f;
	public static int scoreRed; 
	public Camera mainCam; 

    // Use this for initialization
    void Start () {
		Vector3 tempPos = gameObject.transform.position;
		tempPos = new Vector3 (-0.75f, -1.03f, 0f);
		gameObject.transform.position = tempPos; 
		scoreRed = 0;
	}
	
	// Update is called once per frame
	void Update () {



		reSpawn (); 


		if (Input.GetKey(KeyCode.A))
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.D))
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.W))
		{
			transform.position += Vector3.up * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.S))
		{
			transform.position += Vector3.down * speed * Time.deltaTime;
		}
    }


	void OnCollisionEnter2D(Collision2D other) {

		if (other.gameObject.CompareTag("TimeBonus"))
		{
			Sound.playSound=true; 
			mainCam.GetComponent<GUIscript>().timeLeft += 10f; 
			Destroy(other.gameObject); 
		}
	}
	

	IEnumerator particlesStop(){
		yield return new WaitForSeconds(1f);
		//particles.Stop(); 
	}


	void reSpawn(){

		if (gameObject.transform.position.y < -1.7f) {
			
			Vector3 positionTemp = gameObject.transform.position; 
			positionTemp.y = -1.035712f; 
			gameObject.transform.position = positionTemp; 
			gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 0f); 
			
			//FLICKER TRANSPARANCY 
		} 
	}




}
