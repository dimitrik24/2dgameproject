﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject collectTruck; 
	public GameObject firetruck; 
	public GameObject police; 
	public GameObject civilian; 
	public GameObject timeBonus; 
	public static int score; 



	// Use this for initialization
	void Start () {
		score = 0; 
			InvokeRepeating ("spawnCollectTruck", 1.2f, 3f); 
			InvokeRepeating ("spawnFireTruck", 2.0f, 6f); 
			InvokeRepeating ("spawnPoliceCar", 2.5f, 5f); 
			InvokeRepeating ("spawnCivilian", 4f, 10f); 
			InvokeRepeating ("spawnTimeBonus", 10f, 20f); 

	}
	
	// Update is called once per frame
	void Update () {
		           
	}

	void spawnCollectTruck(){
		Instantiate(collectTruck, new Vector3( Random.Range(-1.376f, 1.376f), 1.4f, transform.position.z), Quaternion.identity);
	}

	void spawnFireTruck(){
		Instantiate (firetruck, new Vector3 (Random.Range (-1.376f, 1.376f), 1.4f, transform.position.z), Quaternion.identity);
	}

	void spawnPoliceCar(){
		Instantiate (police, new Vector3 (Random.Range (-1.376f, 1.376f), 1.4f, transform.position.z), Quaternion.identity);
	}

	void spawnCivilian(){
		Instantiate (civilian, new Vector3 (Random.Range (-1.376f, 1.376f), 1.4f, transform.position.z), Quaternion.identity);
	}

	void spawnTimeBonus(){
		Object bob = Instantiate (timeBonus, new Vector3 (Random.Range (-1.376f, 1.376f), 1.4f, transform.position.z), Quaternion.identity) as Object;
		Destroy (bob, 2.5f); 
	}



}
