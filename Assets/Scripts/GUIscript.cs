﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIscript : MonoBehaviour {


	public Text displayTime; 
	private float increment; 
	public float timeLeft = 60; 
	public GameObject gameOver; 
	public GameObject startOver; 
	
	// Use this for initialization
	void Start () {
		Time.timeScale=1;
	}
	
	// Update is called once per frame
	void Update () {
		increment++; 

		timeLeft -=  Time.deltaTime;
		if (timeLeft < 15f) {
			displayTime.color = Color.red; 
			Color temp = displayTime.color;
			temp.a = Mathf.Sin(increment)*1.2f;
			displayTime.color = temp; 
		}
		displayTime.text = "Time Left: " + (int) timeLeft + "\nScore:" + EnemySpawner.score; 

		if (timeLeft < 0.5f) {
		
			gameOver.SetActive(true); 
			startOver.SetActive(true); 
			Time.timeScale=0; 
			//Application.Quit();
			//Application.LoadLevel(Application.loadedLevel);
		}

	}

	public void restartGame(){
		Time.timeScale=1;
		Application.LoadLevel (Application.loadedLevel); 
		 

	}
}
