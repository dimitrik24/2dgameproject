﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {

	public static bool playSound =false; 
	public AudioSource sound; 
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (playSound) {
			sound.Play (); 
			playSound=false; 
		}

	
	}
	
}
